-- Pull in the wezterm API
local wezterm = require 'wezterm'

-- This will hold the configuration.
local config = wezterm.config_builder()

config.font = wezterm.font 'FiraCode Nerd Font'
config.color_scheme = 'Gruvbox dark, soft (base16)'
config.window_background_opacity = 0.95
config.adjust_window_size_when_changing_font_size = false
config.hide_tab_bar_if_only_one_tab = true
config.tab_bar_at_bottom = true
config.use_fancy_tab_bar = false

config.command_palette_rows = 15

config.initial_cols = 120
config.initial_rows = 36

config.keys = {
    {
        key = '|',
        mods = 'CTRL|SHIFT',
        action = wezterm.action.SplitHorizontal
    },
    {
        key = '"',
        mods = 'CTRL|SHIFT',
        action = wezterm.action.SplitVertical
    },
    {
        key = 'q',
        mods = 'CTRL|SHIFT',
        action = wezterm.action.CloseCurrentPane { confirm = true },
    },
    {
        key = 's',
        mods = 'CTRL|SHIFT',
        action = wezterm.action.PaneSelect {
            mode = 'SwapWithActive'
        }
    },


    -- Moving windows with hjkl
    {
        key = 'h',
        mods = 'CTRL|SHIFT',
        action = wezterm.action.ActivatePaneDirection 'Left',
    },
    {
        key = 'l',
        mods = 'CTRL|SHIFT',
        action = wezterm.action.ActivatePaneDirection 'Right',
    },
    {
        key = 'k',
        mods = 'CTRL|SHIFT',
        action = wezterm.action.ActivatePaneDirection 'Up',
    },
    {
        key = 'j',
        mods = 'CTRL|SHIFT',
        action = wezterm.action.ActivatePaneDirection 'Down',
    },

    -- Resizing windows with hjkl
    {
        key = 'h',
        mods = 'CTRL|SHIFT|ALT',
        action = wezterm.action.AdjustPaneSize { 'Left', 5},
    },
    {
        key = 'l',
        mods = 'CTRL|SHIFT|ALT',
        action = wezterm.action.AdjustPaneSize { 'Right', 5},
    },
    {
        key = 'k',
        mods = 'CTRL|SHIFT|ALT',
        action = wezterm.action.AdjustPaneSize { 'Up', 5},
    },
    {
        key = 'j',
        mods = 'CTRL|SHIFT|ALT',
        action = wezterm.action.AdjustPaneSize { 'Down', 5},
    }
}

-- and finally, return the configuration to wezterm
return config
