#!/bin/bash

sudo pacman -S bspwm sxhkd compton feh network-manager-applet dmenu xorg-xsetroot wmname cbatticon flameshot
yay -S polybar pa-applet-git betterlockscreen
