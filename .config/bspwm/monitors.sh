#!/bin/bash

no_of_monitors=$(xrandr --listactivemonitors | awk 'NR == 1 { print $NF }')

killall -9 polybar
echo "running the script"

if [ "$no_of_monitors" == 1 ]; then
    bspc monitor -d 1 2 3 4 5 6 7 8 9 10
    polybar bar-primary &
    echo "running the script for one monitor"
else
    if [ "$HOSTNAME" == "arch" ]; then # desktop
        xrandr --output HDMI-1 --mode 1920x1080 --rate 74.97 --output DP-2 --mode 1920x1080 --rate 59.93 --left-of HDMI-1

        bspc monitor DP-2 -d 1 3 5 7 9
        bspc monitor HDMI-1 -d 2 4 6 8 10

        MONITOR=DP-2 polybar bar-primary &
        MONITOR=HDMI-1 polybar bar-secondary &
    elif [ "$HOSTNAME" == "archlinux" ]; then #laptop
        MONITOR=eDP-1 polybar bar-primary &
        MONITOR=HDMI-1 polybar bar-secondary &
        bspc monitor eDP-1 -d 1 2 3 4 5 6 7 8 9

        xrandr --output HDMI-1 --mode 1920x1080 --rate 60.00 --output eDP-1 --mode 1920x1080 --rate 60.03 --right-of HDMI-1
        bspc monitor eDP-1 -d 1 3 5 7 9
        bspc monitor HDMI-1 -d 2 4 6 8 10
    fi
fi

echo "finishing the script"
