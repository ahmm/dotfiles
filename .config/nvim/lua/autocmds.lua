local augroup = vim.api.nvim_create_augroup("MyGroup", { clear = true })

vim.api.nvim_create_autocmd("TextYankPost", {
    desc = "Highlight yanked region to give visual feedback",
    group = augroup,
    callback = function()
        vim.highlight.on_yank()
    end,
})
