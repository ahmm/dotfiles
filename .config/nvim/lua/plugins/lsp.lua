local setup_lsp_keybindings = function(_, bufnr)
    local nmap = function(keys, func, desc)
        if desc then
            desc = "LSP: " .. desc
        end

        vim.keymap.set("n", keys, func, { buffer = bufnr, desc = desc })
    end

    nmap("K", vim.lsp.buf.hover)
    nmap("gd", vim.lsp.buf.definition, "Go to definition")
    nmap("gr", vim.lsp.buf.references, "Find references")
    nmap("gI", vim.lsp.buf.implementation, "Go to implementation")

    vim.keymap.set("i", "<C-h>", vim.lsp.buf.signature_help, { buffer = bufnr })

    nmap("<leader>ca", vim.lsp.buf.code_action, "Code actions")

    nmap("<leader>cr", require("telescope.builtin").lsp_references, "References")

    nmap("<leader>cR", vim.lsp.buf.rename, "Rename")
    nmap("<leader>cf", vim.lsp.buf.format, "Format file")

    nmap("<leader>cs", require("telescope.builtin").lsp_document_symbols, "Document symbols")
    nmap("<leader>cS", require("telescope.builtin").lsp_workspace_symbols, "Workspace symbols")
    nmap("<leader>fS", require("telescope.builtin").lsp_workspace_symbols, "Workspace symbols")
end

return {
    "VonHeikemen/lsp-zero.nvim",
    branch = "v2.x",
    dependencies = {
        -- LSP Support
        { "neovim/nvim-lspconfig" }, -- Required
        { "williamboman/mason.nvim" }, -- Optional
        { "williamboman/mason-lspconfig.nvim" }, -- Optional

        -- Autocompletion
        { "hrsh7th/nvim-cmp" }, -- Required
        { "hrsh7th/cmp-nvim-lsp" }, -- Required
        { "hrsh7th/cmp-buffer" }, -- Optional
        { "hrsh7th/cmp-path" }, -- Optional
        { "hrsh7th/cmp-nvim-lua" }, -- Optional

        -- Snippets
        { "L3MON4D3/LuaSnip" }, -- Required

        -- Sources other than lsp
        { "jose-elias-alvarez/null-ls.nvim" },
    },
    config = function()
        local cmp = require("cmp")
        local null_ls = require("null-ls")
        local lsp_zero = require("lsp-zero").preset({})

        vim.diagnostic.config({
            virtual_text = false,
        })

        -- We explicitly set the key bindings here
        lsp_zero.on_attach(setup_lsp_keybindings)

        -- configure lua language server  for neovim
        lsp_zero.nvim_workspace()

        lsp_zero.setup()

        -- settings for cmp
        local cmp_select = { behavior = cmp.SelectBehavior.Select }
        cmp.setup({
            mapping = {
                -- Move through the list by C-p / C-n
                ["<C-p>"] = cmp.mapping.select_prev_item(cmp_select),
                ["<C-n>"] = cmp.mapping.select_next_item(cmp_select),
                -- Insert current complition
                ["<Tab>"] = cmp.mapping.confirm({ select = true }),
                -- Initialize completion menu when it is not shown
                ["<C-space>"] = cmp.mapping.complete(),
                -- Unset S-Tab as it doesn't make sense now
                ["<S-Tab"] = vim.NIL,
                -- Unset Enter so that it doesn't complete
                ["<CR>"] = vim.NIL,
            },
        })

        -- configuring sources from null-ls
        null_ls.setup({
            sources = {
                null_ls.builtins.formatting.black,
                null_ls.builtins.diagnostics.ruff,
                null_ls.builtins.formatting.stylua,
            },
        })
    end,
}
