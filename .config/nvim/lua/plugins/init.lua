return {
    -- color schemes
    {
        "ellisonleao/gruvbox.nvim",
        config = function()
            require("gruvbox").setup({
                contrast = "soft",
                overrides = {
                    SignColumn = { bg = "" },
                    NormalFloat = { bg = "#32302f" },
                },
                transparent_mode = true,
            })
            vim.cmd("colorscheme gruvbox")
        end,
    },

    -- which key increases discoverability by displaying key candidates when typing comands
    {
        "folke/which-key.nvim",
        config = function()
            require("which-key").setup()
            require("which-key").add({
                { "<leader>f", group = "[F]ind" },
                { "<leader>c", group = "[C]ode" },
                { "<leader>g", group = "[G]it" },
                { "<localleader>", group = "local" },
            })
        end,
    },

    -- status bar
    {
        "nvim-lualine/lualine.nvim",
        dependencies = { "nvim-tree/nvim-web-devicons" },
        opts = {
            options = {
                theme = "gruvbox",
                icons_enabled = true,
                component_separators = { left = "", right = "" },
                section_separators = { left = "", right = "" },
            },
            sections = {
                lualine_c = {
                    { "filename", path = 1, padding = 0 },
                },
                lualine_x = { "filetype", "encoding", "fileformat" },
            },
            extensions = { "oil", "lazy" },
        },
    },

    -- telescope provides ui for fuzzy finding files, buffers and so on
    {
        "nvim-telescope/telescope.nvim",
        lazy = false,
        version = "*",
        dependencies = {
            "nvim-lua/plenary.nvim",
            {
                "nvim-telescope/telescope-fzf-native.nvim",
                build = "make",
            },
        },
        config = function()
            local telescope = require("telescope")
            local actions = require("telescope.actions")

            telescope.setup({
                defaults = {
                    mappings = {
                        i = {
                            ["<esc>"] = actions.close,
                        },
                    },
                },
            })

            require("telescope").load_extension("fzf")
        end,
    },

    -- git
    {
        "lewis6991/gitsigns.nvim",
        config = true,
    },
    {
        "NeogitOrg/neogit",
        event = "VeryLazy",
        dependencies = { "sindrets/diffview.nvim" },
        opts = {
            integrations = {
                diffview = true,
            },
        },
    },

    -- oil file explorer
    {
        "stevearc/oil.nvim",
        dependencies = { "nvim-tree/nvim-web-devicons" },
        config = function()
            require("oil").setup({
                keymaps = {
                    ["<C-v>"] = "actions.select_vsplit",
                    ["<C-x>"] = "actions.select_split",
                    ["<C-r>"] = "actions.refresh",
                },
            })
        end,
    },

    -- treesitter
    {
        "nvim-treesitter/nvim-treesitter",
        build = ":TSUpdate",
        config = function()
            require("nvim-treesitter.configs").setup({
                -- A list of parser names, or "all" (the five listed parsers should always be installed)
                ensure_installed = {
                    "c",
                    "go",
                    "javascript",
                    "lua",
                    "markdown",
                    "markdown_inline",
                    "python",
                    "query",
                    "rust",
                    "vim",
                    "vimdoc",
                },
                -- Install parsers synchronously (only applied to `ensure_installed`)
                sync_install = false,
                -- Automatically install missing parsers when entering buffer
                -- Recommendation: set to false if you don't have `tree-sitter` CLI installed locally
                auto_install = true,
                highlight = {
                    enable = true,
                    additional_vim_regex_highlighting = { "org" },
                },
            })
        end,
    },

    { "nvim-treesitter/playground" },

    -- Display LSP symbols in a tree
    {
        "simrat39/symbols-outline.nvim",
        opts = {
            autofold_depth = 0,
        },
    },

    -- Plugin for smart commenting regions of code
    {
        "numToStr/Comment.nvim",
        config = true,
    },

    {
        "windwp/nvim-autopairs",
        event = "InsertEnter",
        opts = {},
    },

    -- Markdown preview
    {
        "iamcco/markdown-preview.nvim",
        config = function()
            vim.fn["mkdp#util#install"]()
        end,
    },

    {
        "jpalardy/vim-slime",
        init = function()
            vim.g.slime_target = "wezterm"
            vim.g.slime_default_config = { pane_direction = "next" }
        end,
    },

    {
        "tpope/vim-surround",
    },

    {
        "stevearc/aerial.nvim",
        config = function()
            require("aerial").setup({
                -- optionally use on_attach to set keymaps when aerial has attached to a buffer
                on_attach = function(bufnr)
                    -- Jump forwards/backwards with '{' and '}'
                    vim.keymap.set("n", "{", "<cmd>AerialPrev<CR>", { buffer = bufnr })
                    vim.keymap.set("n", "}", "<cmd>AerialNext<CR>", { buffer = bufnr })
                end,
                layout = {
                    default_direction = "prefer_left",
                },
                attach_mode = "global",
            })
            -- You probably also want to set a keymap to toggle aerial
            vim.keymap.set("n", "<leader>a", "<cmd>AerialToggle!<CR>")
        end,
    },

    -- {
    --     "shortcuts/no-neck-pain.nvim",
    --     opts = {
    --         width = 110,
    --         minSideBufferWidth = 30,
    --         autocmds = {
    --             enableOnVimEnter = true,
    --             enableOnTabEnter = true,
    --         },
    --         buffers = {
    --             wo = {
    --                 fillchars = "eob: ",
    --             },
    --         },
    --     },
    -- },
}
