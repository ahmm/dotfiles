--- Basics
-- Don't create backup or swap files
vim.opt.writebackup = false
vim.opt.swapfile = false

vim.opt.completeopt = "menuone,noselect"

-- Set up relative line numbers (both settings are necessary)
vim.opt.number = true
vim.opt.relativenumber = true

-- Highlight the current line
vim.opt.cursorline = true

-- Open splits to the right/bottom of the current one
vim.opt.splitright = true
vim.opt.splitbelow = true

-- Don't highlight on search, so that you don't have to :noh
vim.opt.hlsearch = false

-- Enable 24-bit colors in the terminal
vim.opt.termguicolors = true
vim.opt.background = "dark"

-- Show invisible characters like new lines, tabs, and trailing whitespace
vim.opt.list = true
vim.opt.showbreak = "↪"
vim.opt.listchars = { tab = "» ", nbsp = "·", trail = "·", extends = "›", precedes = "‹" }

-- Folding
-- Start with all folds open
vim.opt.foldlevelstart = 99
vim.opt.foldenable = false

-- Use treesitter for folding by default
vim.opt.foldmethod = "expr"
vim.opt.foldexpr = "nvim_treesitter#foldexpr()"

-- Don't need to show current mode at the bottom with status line plugins
vim.opt.showmode = false

-- Tabs
vim.opt.expandtab = true -- replace tabs with n spaces
vim.opt.tabstop = 4      -- set <TAB> to 4 spaces by default
vim.opt.shiftwidth = 4   -- number of space to use for autoindent, >> etc.

-- Make search case insensitive unless capital letters are used
vim.opt.ignorecase = true
vim.opt.smartcase = true

-- Time after which CursorHold autocmd wil lbe called.
-- This leads to a better experience with some plugins, like gitgutter.
vim.opt.updatetime = 300

-- How long to wait for next char in a sequence when typing. Useful for which-key.
vim.opt.timeoutlen = 500

-- Number of lines to keep when scrolling
vim.opt.scrolloff = 8

-- Always show the sign column, so that it doesn't jump around with diagnostics
vim.opt.signcolumn = "yes"

-- Set leader key to space and local leader to ,
vim.g.mapleader = " "
vim.g.maplocalleader = ","


-- Disabling netrw, because we want to use `oil` instead
vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1


-- gui settings
if vim.g.neovide then
  vim.g.neovide_cursor_animation_length = 0
end
