local reload_package = function(name)
    package.loaded[name] = nil
end

R = function(name)
    reload_package(name)
end
