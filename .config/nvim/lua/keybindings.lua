-- Unbind Q
vim.keymap.set("n", "Q", "<nop>")

-- Diagnostics
vim.keymap.set("n", "[d", vim.diagnostic.goto_prev, { desc = "Previous diagnostic" })
vim.keymap.set("n", "]d", vim.diagnostic.goto_next, { desc = "Next diagnostic" })
vim.keymap.set("n", "<leader>cd", vim.diagnostic.setqflist, { desc = "Diagnostics list" })
vim.keymap.set("n", "<leader>cD", vim.diagnostic.open_float, { desc = "Diagnostic message under the cursor" })

-- Find
local builtin = require("telescope.builtin")
vim.keymap.set("n", "<leader>ff", builtin.find_files, { desc = "Find files" })
vim.keymap.set("n", "<leader>fg", builtin.git_files, { desc = "Find in Git ls-files" })
vim.keymap.set("n", "<leader>fs", builtin.live_grep, { desc = "Grep files" })
vim.keymap.set("n", "<leader>fr", builtin.oldfiles, { desc = "Recent files" })
vim.keymap.set("n", "<leader>fn", function()
    builtin.find_files({ search_dirs = { "~/Notes" } })
end, { desc = "Find in notes" })

vim.keymap.set(
    "n",
    "<leader>fc",
    "<cmd>Telescope find_files search_dirs=~/.config/nvim/<cr>",
    { desc = "Config files" }
)

vim.keymap.set("n", "<leader>h", builtin.help_tags, { desc = "Find help tags" })

-- git
vim.keymap.set("n", "<leader>gb", builtin.git_branches, { desc = "Browse branches" })
vim.keymap.set("n", "<leader>gc", builtin.git_commits, { desc = "Browse commits" })
vim.keymap.set("n", "<leader>gs", "<cmd>:Neogit<cr>", { desc = "Neogit Status" })

-- search open buffers
vim.keymap.set("n", "<leader><Space>", function()
    builtin.buffers({ sort_lastused = true, ignore_current_buffer = true })
end, { desc = "Search open buffers" })

-- yank/paste to/from system clipboard
vim.keymap.set({ "n", "v" }, "<leader>y", [["+y]], { desc = "Yank to system clipboard" })
vim.keymap.set({ "n", "v" }, "<leader>p", [["+p]], { desc = "Paste from system clipboard" })

--- Misc
vim.keymap.set("n", "<leader>+", "<cmd>let @+=@<cr>", { desc = "Copy unnamed register to clipboard" })

vim.keymap.set("n", "-", "<cmd>Oil<cr>", { desc = "Open parent directory" })

-- Abbreviations
-- Type :vh to open help in vertical window
vim.cmd("cnoreabbrev vh vert help")
