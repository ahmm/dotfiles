vim.opt_local.number = false
vim.opt_local.relativenumber = false
vim.opt_local.conceallevel = 2
vim.opt_local.shiftwidth = 2
vim.opt_local.tabstop = 2

-- substitues fname -> fname.md so that gf works for following links in markdown files
vim.opt_local.suffixesadd:append({ ".md", ".mkd" })
