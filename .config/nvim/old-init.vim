" ====================
" General config {{{
" ====================
" let vim detect filetypes, load plugins and let them handle indentation filetype plugin indent on

syntax enable  " enable syntax highlighting

set hidden  " don't need to write current buffer in order to switch
set nobackup nowritebackup noswapfile  " Don't make backup or swap files
set autoread " automatically reload file if has been changed outside of vim

set cursorline      " highlight current line

set clipboard=unnamedplus  " put every yank etc. in clipboard

" List invisible characters like tabs and trailing whitespace
set list
set showbreak=↪
set listchars=tab:»\ ,extends:›,precedes:‹,nbsp:·,trail:·

set splitbelow splitright " Open splits to the right/bottom which is more intuitive

set incsearch   " show partial matches
set hlsearch    " highlight all matches
set ignorecase  " ignore case when searching, necessary for smartcase
set smartcase   " ignore case when lowercase, respect case for uppercase

set number relativenumber

" Tabs, spaces, indent
set expandtab     " use tabs instead of spaces
set shiftwidth=4  " size of autoindent, indenting with '>>' etc
set tabstop=4     " number of spaces that tab in the file counts for
set autoindent    " keep indent (for plaintext)

" set foldmethod=indent " Fold code by indent, 
set foldlevelstart=99 " start with all folds open

set noshowmode " don't need to show the current mode at the bottom because of lightline

" Time after which CursorHold autocommand will be called.
" This leads to a better experience with some plugins, like gitgutter. 
set updatetime=300

" }}}
" ====================
" Plugins {{{
" ====================
" install vimplug first - https://github.com/junegunn/vim-plug
call plug#begin('~/.config/nvim/plugged')

" Colorschemes
Plug 'morhetz/gruvbox'
Plug 'arcticicestudio/nord-vim'

" Lightline
Plug 'itchyny/lightline.vim'

" Fzf for fuzzy searching
Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim'

" Easy motion
Plug 'easymotion/vim-easymotion'

" Markdown
Plug 'plasticboy/vim-markdown'

" Snippets
Plug 'SirVer/ultisnips'

" Syntax files
Plug 'sheerun/vim-polyglot'

" Language server integration:
Plug 'neovim/nvim-lspconfig'
Plug 'hrsh7th/nvim-compe'

" Treesitter
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}

" Display git diff in the sign column:
Plug 'airblade/vim-gitgutter'

" Automatically encrypt/decrypt files with .gpg, .pgp, .asc extensions:
Plug 'jamessan/vim-gnupg'

" Distraction-free writing
Plug 'junegunn/goyo.vim'

" This plugin, combined with a series of tmux keybindings allows for moving
" between vim splits and tmux panes with ctrl-h etc.
" Plug 'christoomey/vim-tmux-navigator'

" The following plugins I may delete in the future:
" Tagbar - for something lsp based?
Plug 'preservim/tagbar'
Plug 'machakann/vim-highlightedyank'

call plug#end()

" }}}
" ====================
" Key bindings {{{
" ====================
let mapleader = " "
let maplocalleader = ''

" Keep cursor position when searching with '*'
nnoremap* *<c-o>

" Smart way to move between splits
nnoremap <C-h> <C-W>h
nnoremap <C-j> <C-W>j
nnoremap <C-k> <C-W>k
nnoremap <C-l> <C-W>l

" Remove search highlight
nnoremap <esc> :nohlsearch<cr>

" FZF
nnoremap <leader><Space> :Buffers<CR>
nnoremap <leader>ff :Files<CR>
nnoremap <leader>fg :GFiles<CR>
nnoremap <leader>fr :Rg<CR>

" Use <esc> to exit insert mode in the terminal buffer
tnoremap <Esc> <C-\><C-n>

" Unbind Q so that you don't enter the :ex mode by accident
nnoremap Q <nop>

" }}}
" ====================
" Appearance {{{
" ====================
" Enable 24-bit colors in the terminal
set termguicolors 

colorscheme nord
set background=dark

let g:highlightedyank_highlight_duration = 100
highlight HighlightedyankRegion cterm=reverse gui=reverse

" Fix for transparency in the terminal:
"hi Normal guibg=NONE ctermbg=NONE

let g:gruvbox_italic = '1'  " enable italic text

let g:lightline = {
  \ 'colorscheme': 'nord',
  \}

" }}}
" ====================
" Abbreviations {{{
" ====================
" Type :vh to open help in vertical window
cnoreabbrev vh vert help
iabbrev ipdb; import ipdb; ipdb.set_trace(context=10)

" }}}
" ====================
" Language-specific {{{
" ====================
let g:python_host_prog = '/usr/bin/python2'
let g:python3_host_prog = '/usr/bin/python3'

" augroup python_settings
"     autocmd!
"     autocmd BufNewFile,BufRead *.py set foldmethod=indent
" augroup END

" Make vim highlight everything it can for python:
let python_highlight_all = 1

augroup web_settings
    autocmd!
    autocmd BufNewFile,BufRead *.js,*.ts,*.html,*.css,*.scss
          \ set tabstop=2
          \ softtabstop=2
          \ shiftwidth=2
augroup END

augroup lua
    autocmd!
    autocmd BufNewFile,BufRead *.lua set tabstop=2 softtabstop=2 shiftwidth=2
augroup END

" }}}
" ====================
" Netrw {{{
" ====================
" Hide the netrw banner
let g:netrw_banner = 0

" Don't show hidden files by default:
let g:netrw_list_hide = '(^\|\)\zs\.+'

" Use -r to allow for deleting non-empty directories (doesn't seem to work)
let g:netrw_localrmdir='rm -r'

augroup netrw_mappings
    autocmd!
    " Hack to make ctrl-l work property in netrw
    autocmd filetype netrw noremap <buffer> <C-l> <C-w>l
augroup END

" }}}
" ====================
" Easy Motion {{{
" ====================
let g:EasyMotion_do_mapping = 0 " Disable default mappings

" Jump based on typing two characters (even across windows)
nmap s <Plug>(easymotion-overwin-f2)

" It doesn't make sense to jump across the windows in visual & operator
" pending modes:
vmap s <Plug>(easymotion-s2)
omap s <Plug>(easymotion-s2)


" Turn on case-insensitive feature
let g:EasyMotion_smartcase = 1

" }}}
" ====================
" FZF {{{
" ====================
"
function! s:insert_filename_without_extension(lines)
  let @z = fnamemodify(a:lines[0], ':r')
  normal! "zp
endfunction

let g:fzf_action = {
    \ 'ctrl-i': function('s:insert_filename_without_extension'),
    \ 'ctrl-t': 'tab split',
    \ 'ctrl-x': 'split',
    \ 'ctrl-v': 'vsplit' }


" }}}
" ====================
" UltiSnips{{{
" ====================
" Trigger snippet completion with c-j in insert mode
let g:UltiSnipsExpandTrigger="<c-j>"

" Edit snippet file for current filetype:
nnoremap <leader>se :UltiSnipsEdit<cr>

" }}}
" ====================
" Markdown/Note taking {{{
" ====================
autocmd BufEnter * if &filetype == "" | setlocal ft=markdown | endif

" Follow links without md extension:
let g:vim_markdown_no_extensions_in_markdown = 1

" Follow named anchors like file#anchor
let g:vim_markdown_follow_anchor = 1

" You can jump from this-is-title to 'This is title'
let g:vim_markdown_anchorexpr = 'substitute(v:anchor, "-", " ", "g")'

" Allow toml frontmatter (metadata) for hugo etc.
let g:vim_markdown_frontmatter = 1

" Don't conceal code blocks:
let g:vim_markdown_conceal_code_blocks = 0

augroup MarkdownGroup
    autocmd!
    " Hide special stuff like *'s around bold, or links unless you're on the line
    " 2 means that the concealed characters are hidden entirely instead of 
    " being replaced by spaces
    autocmd FileType markdown setlocal conceallevel=2
    autocmd FileType markdown setlocal textwidth=79
    autocmd BufNewFile,BufRead *.md set tabstop=2 softtabstop=2 shiftwidth=2
augroup END


nnoremap <leader>zz :edit ~/Nextcloud/Notes/_index.md<cr>
nnoremap <leader>zi :edit ~/Nextcloud/Notes/inbox.md<cr>

command! -nargs=1 ZettelNew :execute ":edit" . strftime('%Y%m%d%H%M%S') . "-<args>.md"
nnoremap <leader>zn :ZettelNew<space>

nnoremap <leader>zf :Files ~/Nextcloud/Notes/<cr>



" Save encrypted files in a text format
let g:GPGPreferArmor=1

" Protect files with password rather than private/public key pair
let g:GPGPreferSymmetric=1

" }}}
" ====================
" Completion {{{
" ====================

" This governs the insert mode completion. Always show menu (even if there is
" only one option) and don't select for the user. Necassary for compe
set completeopt=menuone,noselect

" Suppress completion messages like 'The only match'
set shortmess+=c

let g:completion_matching_strategy_list = ['exact', 'substring', 'fuzzy']

" Always show the sign columns so that it doesn't jump with new diagnostic
"set signcolumn=yes

" Show the sign column in the same color as the background:
:highlight clear SignColumn

nnoremap <silent> gd <cmd>lua vim.lsp.buf.definition()<CR>
"nnoremap <silent> gD <cmd>lua vim.lsp.buf.declaration()<CR>
nnoremap <silent> gr <cmd>lua vim.lsp.buf.references()<CR>
nnoremap <silent> gR <cmd>lua vim.lsp.buf.rename()<CR>
nnoremap <silent> g0 <cmd>lua vim.lsp.buf.document_symbol()<CR>
" vim.lsp.buf.code_action()

" Navigate diagnostics from lsp
nnoremap <silent> ]e <cmd>lua vim.lsp.diagnostic.goto_next()<CR>
nnoremap <silent> [e <cmd>lua vim.lsp.diagnostic.goto_prev()<CR>

nnoremap <silent> K :call <SID>show_documentation()<CR>
function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    lua vim.lsp.buf.hover()
  endif
endfunction



" Give diagnostic yellow/red colors so that they stand out
if exists('+termguicolors')
    let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
    let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
endif

lua << EOF
vim.lsp.handlers["textDocument/publishDiagnostics"] = vim.lsp.with(
  vim.lsp.diagnostic.on_publish_diagnostics, {
    virtual_text = false,
  }
)
EOF

" autocmd CursorHold <buffer> lua vim.lsp.buf.document_highlight()
" autocmd CursorHoldI <buffer> lua vim.lsp.buf.document_highlight()
" autocmd CursorMoved <buffer> lua vim.lsp.buf.clear_references()

augroup lsp_highlight_symbol_under_cursor
    autocmd!
    autocmd CursorHold * lua vim.lsp.buf.document_highlight()
    autocmd CursorMoved * lua vim.lsp.buf.clear_references()
augroup END


lua << EOF
require'lspconfig'.pyright.setup{}
require'lspconfig'.rls.setup{}
EOF

lua << EOF
require'compe'.setup {
  enabled = true;
  autocomplete = true;
  debug = false;
  min_length = 2;
  preselect = 'enable';
  throttle_time = 80;
  source_timeout = 200;
  incomplete_delay = 400;
  max_abbr_width = 100;
  max_kind_width = 100;
  max_menu_width = 100;
  documentation = true;

  source = {
    path = true;
    buffer = true;
    calc = true;
    nvim_lsp = true;
    nvim_lua = true;
    vsnip = true;
  };
}
EOF

inoremap <silent><expr> <C-Space> compe#complete()
inoremap <silent><expr> <CR>      compe#confirm('<CR>')

set foldmethod=expr
set foldexpr=nvim_treesitter#foldexpr()

lua << EOF
require'nvim-treesitter.configs'.setup {
  -- A list of parser names, or "all"
  ensure_installed = { "rust", "json", "python", "javascript" },
  highlight = {
    enable = true,
    disable = { "markdown" },
  },
  incremental_selection = {
      enable = true,
      keymaps = {
        init_selection = "gnn",
        node_incremental = "grn",
        scope_incremental = "grc",
        node_decremental = "grm",
      }
    },
  indent = {
    enable = true
  },
}
EOF


let g:tagbar_ctags_bin='/opt/homebrew/bin/ctags'

" }}}
" ====================
" Learning vimscript {{{
" ====================

" Match tralining whitespace as an error
" match Error /\s\+$/  

" Previous buffer in vsplit:
" nnoremap <leader>^ :execute 'vsplit ' . bufname('#')<cr>

" This is great:
":onoremap i( :<c-u>normal! f(vi(<cr>

" make it better by writing a function that saves previous search and the position:
" autocmd BufWritePre * :%s/\s\s*$//e
" autocmd BufWritePre *.html :normal gg=G

" autocmd FileType html :iabbrev <buffer> -- &mdash;

"}}}
"====================
"Revisit{{{
"====================

"invisual mode move line up/down with proper indent
"vnoremapJ :m '>+1<CR>gv=gv
"vnoremapK :m '<-2<CR>gv=gv

"forpython, run current file and stop in pdb on error:
":terminalipython % -i

"Plugins
"https://github.com/norcalli/nvim-colorizer.lua

"setlocalcolorcolumn=80 do this for python maybe

"setcurrent working dir to project root
"setlocalpath=.,**



"Changethe case of the word before the cursor in insert mode
"inoremap<S-u> <esc>vb~ea

"Changecurrent dir
"cnoreabbrevcd% cd %:p:h






"}}}

" vim: foldmethod=marker
