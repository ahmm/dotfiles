
###################
# General
###################
# change folder without typing 'cd'
setopt AUTO_CD

# Don't print error when globs like '?' are not matched
# This accidentally breaks arguments like urls that have ? in them
unsetopt nomatch

###################
# Vi mode
###################
# Enable vi mode:
bindkey -v

# Make switching between vi modes quicker
export KEYTIMEOUT=1

# Load module which allows for editing the command line in the visual editor
autoload edit-command-line

zle -N edit-command-line

# Type v to edit current command in $EDITOR like in bash
bindkey -M vicmd v edit-command-line

###################
# Completion
###################
# Setup a custom completions directory
fpath=($HOME/.local/share/zsh/completions $fpath)

# Enable the completion system
autoload -Uz compinit

# Initialize all completions on $path and ignore (-i) all insecure files and directories
compinit -i

# Complete dotfiles:
_comp_options+=(globdots)

zstyle ':completion:*' menu select search

zstyle ':completion:*:*:*:*:descriptions' format '%F{green}-- %d --%f'

# Group results for aliases, external command etc. together
zstyle ':completion:*' group-name ''

# Add nice colors for directory and file completion menus (use same colors as ls --color=auto)
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}


###################
# Environment
###################

# Add my scripts directory to path:
export PATH="$HOME/.cargo/bin:$PATH"
export PATH=$HOME/.local/bin:$HOME/.scripts:$PATH

export EDITOR=nvim

# Use neovim as a man pager
export MANPAGER='nvim +Man!'

###################
# History
###################
HISTFILE="$HOME/.cache/.zsh_history"
HISTSIZE=10000 # numbers of command loaded to memory from history file
SAVEHIST=$HISTSIZE # numbers of command stored in zsh history file

# Add commands to history file as they're entered, not on shell exit
setopt INC_APPEND_HISTORY

# Don't save duplicate commands in history
setopt HIST_IGNORE_ALL_DUPS

# Ignore commands that start with whitespace
setopt HIST_IGNORE_SPACE


###################
# Version Control
###################
# Load the version control plugin
autoload -Uz vcs_info

###################
# Prompt
###################
# Left prompt:
export PS1="%B%(?.%F{green}➜.%F{red}➜)%f %F{cyan}%1c%f %F{yellow}✗%f %b" 

precmd_vcs_info() { vcs_info }
precmd_functions+=( precmd_vcs_info )
setopt prompt_subst
RPROMPT=\$vcs_info_msg_0_
zstyle ':vcs_info:git:*' formats '%B%F{blue}(%f%F{red}%b%f%F{blue})%f'
zstyle ':vcs_info:*' enable git

###################
# Aliases
###################
alias ls='ls --color=auto'
alias grep='grep --color=auto'
alias o='xdg-open'

alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'


alias vim='nvim'
alias vi='nvim'
alias v='nvim'

alias fe=fzf_edit_file
alias fcd=fzf_cd_into_file_dir

###################
# Directory stack
###################
# Push the current visited directory to stack
# Then you can use 'cd -<tab>' or dirs -v to use that
setopt AUTO_PUSHD

# Don't print the directory stack on popd
setopt PUSHD_SILENT

setopt PUSHD_IGNORE_DUPS

###################
# Fuzzy find
###################

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
[ -f /usr/share/fzf/shell/key-bindings.zsh ] && source /usr/share/fzf/shell/key-bindings.zsh

# This is a hack to make alt-c binding work on mac (with ⌥-c)
bindkey "ć" fzf-cd-widget

export FZF_DEFAULT_OPTS="--height 75% --color=bg+:#282828,gutter:#282828"
export FZF_DEFAULT_COMMAND='fd --type f --hidden --ignore-file "$HOME/.fdignore" .'
export FZF_ALT_C_COMMAND='fd --type d --full-path "$HOME"'
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"

fzf_cd_into_file_dir () {
  local file
  local dir
  file=$(fd --type f | fzf) 
  dir=$(dirname "$file")
  cd "$dir"
}

fzf_edit_file () {
    local file=$(
        fd --type f --hidden --ignore-file "$HOME/.fdignore" | fzf --preview="bat {} --color=always --theme=ansi-dark" --preview-window=right:70%:wrap
    )
    if [[ -n $file ]]; then
        $EDITOR $file
    fi
}

# Checkout git branch (including remote branches) sorted by most recent commit
fgc() {
  local branches branch
  branches=$(git branch --all --sort=committerdate | grep -v HEAD) &&
  branch=$(echo "$branches" |
           fzf-tmux -d $(( 2 + $(wc -l <<< "$branches") )) +m) &&
  git checkout $(echo "$branch" | sed "s/.* //" | sed "s#remotes/[^/]*/##")
}

# fzf_docker() {
# }

######################
# Functions
#####################
# Make a directory and cd into it
mcd() {
    mkdir -p $1
    cd $1
}

# Cd and ls contents
cd() {
    builtin cd "$@" && ls
}


# nvm increases startup time significantly, therefore we use this trick to load it 
# only after the first invocation:
nvm() {
    unset -f nvm
    export NVM_DIR="$HOME/.nvm"
    [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm
    nvm "$@"
}

node() {
    unset -f node
    export NVM_DIR="$HOME/.nvm"
    [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm
    node "$@"
}

pyenv() {
    unset -f pyenv
    export PATH="$HOME/.pyenv/shims:$PATH"
    pyenv init --path
    pyenv "$@"
}
