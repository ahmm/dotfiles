if status is-interactive
    # Commands to run in interactive sessions can go here
end

###################
# Variables
###################
set -gx EDITOR nvim

# Use nvim as a man pager
set -gx MANPAGER 'nvim +Man!'

fish_add_path /opt/homebrew/bin /opt/homebrew/sbin /usr/local/bin ~/.local/bin
fish_add_path ~/.cargo/bin ~/.npm/bin


###################
# Aliases
###################
alias ls='ls --color=auto'
alias grep='grep --color=auto'
alias o='xdg-open'

alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'

alias vim='nvim'
alias vi='nvim'
alias v='nvim'


###################
# Fuzzy find
###################
set -g FZF_DEFAULT_OPTS "--height 75% --color=bg+:#282828,gutter:#282828"
set -g FZF_DEFAULT_COMMAND 'fd --type f --hidden --ignore-file "$HOME/.fdignore" .'
set -g FZF_ALT_C_COMMAND 'fd --type d --full-path "$HOME"'
set -g FZF_CTRL_T_COMMAND "$FZF_DEFAULT_COMMAND"

if test -f /usr/share/fzf/shell/key-bindings.fish
    source /usr/share/fzf/shell/key-bindings.fish
end

if test -f ~/.fzf/shell/key-bindings.fish
    source ~/.fzf/shell/key-bindings.fish
end

function fish_user_key_bindings
    fzf_key_bindings
end

###################
# Stuff to make it work more like bash/zsh
###################
# allow for `sudo !!`
function sudobangbang --on-event fish_postexec
    abbr -g !! $argv[1]
end

