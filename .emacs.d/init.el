;; Most of the configuration is done in the 'configuration.org' file
;; This file is kept as minimal as possible and it's only purpose is
;; to set up packaging and load the other file via org-babel.


;; Load file with the actual config
(org-babel-load-file (expand-file-name "~/.emacs.d/configuration.org"))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("1a1ac598737d0fcdc4dfab3af3d6f46ab2d5048b8e72bc22f50271fd6d393a00" "467dc6fdebcf92f4d3e2a2016145ba15841987c71fbe675dcfe34ac47ffb9195" "e3daa8f18440301f3e54f2093fe15f4fe951986a8628e98dcd781efbec7a46f2" default))
 '(package-selected-packages
   '(evil-surround flymake-cursor flymake-diagnostic-at-point flymake-popon avy eglot general org-roam yasnippet-snippets yasnippte-snippets yasnippet org-appear org-make-toc writeroom-mode which-key use-package rainbow-delimiters org-superstar org-modern ob-rust all-the-icons)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
(put 'narrow-to-region 'disabled nil)
(put 'dired-find-alternate-file 'disabled nil)
