" Set leader key
let mapleader=' '
let maplocalleader = ','

" Searching
set ignorecase
set smartcase
set hlsearch
set incsearch
set wrapscan

set quickscope
let g:qs_highlight_on_keys = ['f', 'F', 't', 'T']

" Keep cursor position when searching with '*'
nnoremap * *<c-o>

" Remove search highlight
nnoremap <esc> :nohlsearch<cr>

" Buffers and splits
set splitbelow splitright

" Show relative numbers:
set relativenumber
set number

" Join vim global marks and IDE's bookmarks:
set ideamarks

set highlightedyank

" Windows
nmap <c-w>v <Action>(SplitVertically)
nmap <c-w>s <Action>(SplitHorizontally)
nmap <c-w>w <Action>(NextWindow)
nmap <c-w>q <Action>(CloseContent)

" Move between errors/diagnostics
nmap ]d <Action>(GotoNextError)
nmap [d <Action>(GotoPreviousError)
nmap ]g <Action>(VcsShowNextChangeMarker)
nmap [g <Action>(VcsShowPrevChangeMarker)
nmap ]m <Action>(MethodDown)
nmap [m <Action>(MethodUp)

" Yank to system clipboard
set clipboard=unnamedplus

" Uppercase the word before the cursor in insert mode
inoremap <C-u> <esc>vbUea

" Imitate fzf actions in normal vim
nnoremap <leader><Space> :action RecentFiles<cr>
nnoremap <leader>ff :action GotoFile<cr>
nnoremap <leader>fs :action FindInPath<cr>
nnoremap <leader>fd :action GotoDatabaseObject<cr>
nnoremap <leader>a :action GotoAction<cr>
nnoremap <leader>b :action ToggleLineBreakpoint<cr>
nnoremap <leader>B :action EditBreakpoint<cr>
nnoremap <leader>ga :action Annotate<cr>

nnoremap <leader>= :action TabShiftActions.ToggleMaximizeRestore<cr>

vnoremap <leader>gh :action Vcs.ShowHistoryForBlock<cr>


" Plugins
Plug 'tpope/vim-surround'
set surround

Plug 'tpope/vim-commentary'
