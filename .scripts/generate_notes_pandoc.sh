#!/bin/bash

rm -r notes/*

cp -r ~/Nextcloud/Notes/* notes

for f in notes/*.md; do
  filename="${f%.md}.html"
  echo $filename
  pandoc "$f" -s  -c "../mvp.css" -o "$filename" --template template.html --lua-filter=links-to-html.lua --mathjax
done



