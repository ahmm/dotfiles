#!/bin/bash

# TODO: when you buy a hdd start backing up parts of home as well
# TODO: differential backups

DIR_NAME=$(date +%j)

cd /

rsync -aAXv --delete --quiet \
--exclude={"/dev/*","/proc/*","/sys/*","/tmp/*","/run/*","/mnt/*","/media/*","/lost+found","/home","/swapfile"} \
/ "/home/ahmm/.backups/${DIR_NAME}"

cd ..
