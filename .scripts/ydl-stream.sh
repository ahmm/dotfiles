#!/usr/bin/env sh

if [ $# -ne 1 ]; then
     echo "${0#*/}: Wrong number of arguments." >&2
     echo "Usage: ${0#*/} url" >&2
     exit 1
fi

yt-dlp $1 -o - | mpv -

