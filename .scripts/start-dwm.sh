#!/bin/bash

xrandr --output HDMI-1 --mode 1920x1080 --rate 74.97 --output DP-2 --mode 1920x1080 --rate 59.93 --left-of HDMI-1

~/.fehbg &

exec compton &
exec dunst &
#exec dwmblocks &

exec dwm
