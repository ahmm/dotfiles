;; Most of the configuration is done in the configuration.org file
;; This file should be as small as possible

(package-initialize)

;; Don't load installed packages at startup, but on reading this file instead
(setq package-enable-at-startup nil)

;; Add package repository:
(add-to-list 'package-archives
	     '("melpa" .  "https://melpa.org/packages/"))

;; Bootstrap use-package
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

;; Load file with actual config:
(org-babel-load-file (expand-file-name "~/.emacs.d/configuration.org"))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(pyenv-mode hydra yasnippet typescript-mode flymake-eslint emmet-mode company-lsp lsp-ui lsp-mode company dumb-jump rainbow-delimiters highlight-numbers origami org-bullets projectile flx counsel ace-window general evil-matchit evil-goggles evil-collection evil undo-tree comment-dwim-2 expand-region avy diminish spaceline gruvbox-theme deadgrep which-key exec-path-from-shell use-package)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
