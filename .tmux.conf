# Bind prefix ket to ctrl-s instead of default ctrl-b
set-option -g prefix C-s
unbind C-b

# Click C-s again to send it to underlying application:
bind C-s send-prefix

# General settings:
set -g default-shell /bin/fish

# Fixes some issues with terminal colors:
set-option -g default-terminal "screen-256color"

# Renumber windows if one is closed
set-option -g renumber-windows on

# Start numbering at 1
set -g base-index 1

# Allow for faster key repetition
set -s escape-time 0

# Retain current path on splits:
bind % split-window -h -c "#{pane_current_path}"
bind '"' split-window -v -c "#{pane_current_path}"


# Appearance
# Remove content from the left part of the status bar:
set -g status-left "[#S] "

# Move session name to the far right of the status bar
set -g status-right "#{pane_current_path}"
set -g status-right-length 100

# default status bar colors:
set-option -g status-style bg=default,fg="#666666"

# default window color:
set-window-option -g window-status-style bg=default,fg="#666666"

# active window color:
set-window-option -g window-status-current-style bg=default,fg="#fb4934"

# command colors incative
set-option -g message-command-style bg="#666666",fg=default
set-option -g message-style bg="#458588",fg=default

# pane colors:
set-option -g pane-active-border-style fg=colour250 #fg2
set-option -g pane-border-style fg=colour237 #bg1

# colors for all the "interactive" modes
set-option -g mode-style bg="#98971a",fg=default


# hjkl pane traversal
bind h select-pane -L
bind j select-pane -D
bind k select-pane -U
bind l select-pane -R

bind H resize-pane -L 5
bind J resize-pane -D 5
bind K resize-pane -U 5
bind L resize-pane -R 5


# Smart pane switching with awareness of Vim splits and fzf.
# See: https://github.com/christoomey/vim-tmux-navigator
#is_vim="ps -o state= -o comm= -t '#{pane_tty}' \
#    | grep -iqE '^[^TXZ ]+ +(\\S+\\/)?g?(view|n?vim?x?)(diff)?$'"
#
#is_fzf="ps -o state= -o comm= -t '#{pane_tty}' \
#  | grep -iqE '^[^TXZ ]+ +(\\S+\\/)?fzf$'"
#
#bind-key -n 'C-h' if-shell "$is_vim" "send-keys C-h"  "select-pane -L"
#bind-key -n 'C-j' if-shell "$is_vim || $is_fzf" "send-keys C-j"  "select-pane -D"
#bind-key -n 'C-k' if-shell "$is_vim || $is_fzf" "send-keys C-k"  "select-pane -U"
#bind-key -n 'C-l' if-shell "$is_vim" "send-keys C-l"  "select-pane -R"
#bind-key -n 'C-\' if-shell "$is_vim" "send-keys C-\\" "select-pane -l"
#bind-key -T copy-mode-vi 'C-h' select-pane -L
#bind-key -T copy-mode-vi 'C-j' select-pane -D
#bind-key -T copy-mode-vi 'C-k' select-pane -U
#bind-key -T copy-mode-vi 'C-l' select-pane -R
#bind-key -T copy-mode-vi 'C-\' select-pane -l
#
#set-option -g set-titles on






# Hotkeys with alt as mod:
bind-key -n M-= new-window -c "#{pane_current_path}"
bind-key -n M-1 select-window -t :1
bind-key -n M-2 select-window -t :2
bind-key -n M-3 select-window -t :3
bind-key -n M-4 select-window -t :4
bind-key -n M-5 select-window -t :5
bind-key -n M-6 select-window -t :6
bind-key -n M-7 select-window -t :7
bind-key -n M-8 select-window -t :8
bind-key -n M-9 select-window -t :9

bind-key -n M-% split-window -v -c "#{pane_current_path}"
bind-key -n M-\" split-window -h -c "#{pane_current_path}"

bind-key -n M-n select-window -n
bind-key -n M-p select-window -p

bind-key -n M-h select-pane -L
bind-key -n M-l select-pane -R
bind-key -n M-k select-pane -U
bind-key -n M-j select-pane -D

bind-key -n "M-H" run-shell 'old=`tmux display -p "#{pane_index}"`; tmux select-pane -L; tmux swap-pane -t $old'
bind-key -n "M-J" run-shell 'old=`tmux display -p "#{pane_index}"`; tmux select-pane -D; tmux swap-pane -t $old'
bind-key -n "M-K" run-shell 'old=`tmux display -p "#{pane_index}"`; tmux select-pane -U; tmux swap-pane -t $old'
bind-key -n "M-L" run-shell 'old=`tmux display -p "#{pane_index}"`; tmux select-pane -R; tmux swap-pane -t $old'

bind-key -n M-z resize-pane -Z

bind-key -n M-x confirm-before "kill-pane"
bind-key -n M-X confirm-before "kill-window"

bind-key -n M-r command-prompt -I "#W" "rename-window '%%'"

bind-key -n M-< swap-window -t -1
bind-key -n M-> swap-window -t +1
